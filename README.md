# Origami client socket.io helper

## constructor

Params:
 - `privateKey` **required**: private key to encrypt the communication with

## .createFactorySocket

Returns: a promise to be resolved with a factory object or to be rejected. The factory object takes a token parameter to use with the client.
Params:

 - `socket` **required**: a socket object throgh to which establish the communication with.

> Please note that the socket will use several layers. The other end is supposed to be using the [stack socket.io helper](https://gitlab.com/origami2/stack-io)

## .listenSocket

Returns: a promise to be resolved with a API client or to be rejected.
Params:

 - `socket` **required**: a socket object throgh to which establish the communication with.
 - `token` **optional**: the token to use with the client

## .connect

Returns: a promise to be resolved with a API client or to be rejected.
Params:

 - `url` **required**: the url to which to connect to using [socket.io](http://socket.io/)
 - `token` **optional**: the token to use with the client
