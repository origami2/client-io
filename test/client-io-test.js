var ClientIO = require('..');
var Crane = require('origami2').Crane;
var NameRegistry = require('origami2').NameRegistry;
var RSASocket = require('origami2').RSASocket;
var Penelope = require('origami2').Penelope;
var EmittersBus = require('origami2').EmittersBus;
var assert = require('assert');
var testData = {
  "client": {
    "publicKey": "-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAsZo2Yoij0v2OsWQAGMx5dSypNhUNV3FLDBspF3ZtY8Id6lqgeAsH\nZGB9fVhivf2KmXe20teUg5d271U2dbbh3g13AwKacVjVGRR/UB/PUNn0K4jkbFZJ\nJp6nJrwsPlBZrEPUlaJSdveEk0/o4d0OyMNkFWT+Ivuj+DJBQdWQhqsIT2jQKKx5\nVFktJ01K7if3DLaH0fHupqj46zY6bZCRAiyyZynL0blnojwG2tUnRtID6zjOkSB8\ncUKOXHy3jhiTEESwpY8FvJc0HNdG/s/5h0OZ1ayFHRhbeunx6Ddn6Yfewbdix0/H\nBgZ8Ycp3q3fUhVBygQcF+rWEdRTpc1x3UwIDAQAB\n-----END RSA PUBLIC KEY-----",
    "privateKey":"-----BEGIN RSA PRIVATE KEY-----\nMIIEogIBAAKCAQEAsZo2Yoij0v2OsWQAGMx5dSypNhUNV3FLDBspF3ZtY8Id6lqg\neAsHZGB9fVhivf2KmXe20teUg5d271U2dbbh3g13AwKacVjVGRR/UB/PUNn0K4jk\nbFZJJp6nJrwsPlBZrEPUlaJSdveEk0/o4d0OyMNkFWT+Ivuj+DJBQdWQhqsIT2jQ\nKKx5VFktJ01K7if3DLaH0fHupqj46zY6bZCRAiyyZynL0blnojwG2tUnRtID6zjO\nkSB8cUKOXHy3jhiTEESwpY8FvJc0HNdG/s/5h0OZ1ayFHRhbeunx6Ddn6Yfewbdi\nx0/HBgZ8Ycp3q3fUhVBygQcF+rWEdRTpc1x3UwIDAQABAoIBAG0m0AHi5HyJ5SbU\nxNJ46LTBDAX3DkoNkQgRsUGAQV6sMOKVbEYa0SI5wyZEKfuGVaZYUv5mDBqL/ka9\nYkkaatqj/97TvMOoyt8tH8nIowuXyF7xNSH4LeEUZLqDr9VkYJhk38RPDMuc99fp\npsEZsTpmC56ygRZS/cVObZjefoKkl+2cPL0jfSWUSn2t/nUKbmwNbjbM6f1OCCyJ\n814LZalfeLh+hfEdY8yVK0Rlaut/FLw4eaY43H0QTwDyYXmgOgYckia7MQmtu5yO\n0dFRDZa7x0j01w0+Hphr88bhtXFgv8VPWyzyz2d8fNWlbMP4JCARes3lVpkMKHDn\nFZIULcECgYEA53qsuOQrE3xHtNzfak7FjGgkQTxi4TeYXRJ5Zhlkjtb9nQnsFzTq\n+AXpeHfsGmaza9/LcMUJ9JzYV7t5V8T6vZT3riOnjQAfbHOrR3EYK5/vv4ZfjFgk\nBySoNjOtf67ZXn1lRJ3FZNN4RfGmh/2aqYi5PAn1g5+nLXtMIY88a1ECgYEAxGp9\nW03pH4QwBzvFA9PJ+BhWXS8fAcafY67TlXg+IxzGP/YQupodovDChXpvbfYV0kf6\nU6YddDQT5p0j2YmssJCaDa93iMXU2/JgI5pcTNFxLa5LR/Mg30JYyniK/4+YIkoL\n68V2OH0Y65DIQdajHPojzFBmDSBZSnkyO9Ojx2MCgYADCoSMZUr/lYlnoeM5hVFp\nF9EqHj36UX2p810u7zR3//ETCBdW8rYHjiRUFdc/PYwr5aPJln0b/peFB4x/j7Hv\nna5nVkaUPqUrCpX8eUrk/9Ppgz1sHZhTk7K2C5XC8KwgZqtW7G+0dGbHHHagoL9Q\nbOBqHoNgOE+89Dq60iPsEQKBgByahXbueaylS3lCMwbDqP4ATVN0sUdI7Z1OsHFr\n+WCTqCtYYkdKelZoSWu20NNqqvLcmI/l+RQbIWrMJ5RegE+WP1kO3JGGfeEqAuYs\nbJSjS6AjacMonPjmaJfTxipBdx5HOkUzlGvVi/OCOiecYlSt+NigPLxcoaQ+0hn0\nUD2RAoGAUH2QV31iSFLs+ByQdGmPaYuVxhNUwaGoN0M5splIdH6DAgyIvM5ogXVb\nMV3yUdnULU+5YeL8r93NeFRHiBCBfvcIcDdJHxRJNQFvdsAWosGeXI6clC1YkUnB\n2N+9PrizN87XUb6NmY07NwtAKrU6aZ0XwKUEgor9rntT8IawFo4=\n-----END RSA PRIVATE KEY-----"
  },
  "stack": {
    "publicKey": "-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAsZo2Yoij0v2OsWQAGMx5dSypNhUNV3FLDBspF3ZtY8Id6lqgeAsH\nZGB9fVhivf2KmXe20teUg5d271U2dbbh3g13AwKacVjVGRR/UB/PUNn0K4jkbFZJ\nJp6nJrwsPlBZrEPUlaJSdveEk0/o4d0OyMNkFWT+Ivuj+DJBQdWQhqsIT2jQKKx5\nVFktJ01K7if3DLaH0fHupqj46zY6bZCRAiyyZynL0blnojwG2tUnRtID6zjOkSB8\ncUKOXHy3jhiTEESwpY8FvJc0HNdG/s/5h0OZ1ayFHRhbeunx6Ddn6Yfewbdix0/H\nBgZ8Ycp3q3fUhVBygQcF+rWEdRTpc1x3UwIDAQAB\n-----END RSA PUBLIC KEY-----",
    "privateKey":"-----BEGIN RSA PRIVATE KEY-----\nMIIEogIBAAKCAQEAsZo2Yoij0v2OsWQAGMx5dSypNhUNV3FLDBspF3ZtY8Id6lqg\neAsHZGB9fVhivf2KmXe20teUg5d271U2dbbh3g13AwKacVjVGRR/UB/PUNn0K4jk\nbFZJJp6nJrwsPlBZrEPUlaJSdveEk0/o4d0OyMNkFWT+Ivuj+DJBQdWQhqsIT2jQ\nKKx5VFktJ01K7if3DLaH0fHupqj46zY6bZCRAiyyZynL0blnojwG2tUnRtID6zjO\nkSB8cUKOXHy3jhiTEESwpY8FvJc0HNdG/s/5h0OZ1ayFHRhbeunx6Ddn6Yfewbdi\nx0/HBgZ8Ycp3q3fUhVBygQcF+rWEdRTpc1x3UwIDAQABAoIBAG0m0AHi5HyJ5SbU\nxNJ46LTBDAX3DkoNkQgRsUGAQV6sMOKVbEYa0SI5wyZEKfuGVaZYUv5mDBqL/ka9\nYkkaatqj/97TvMOoyt8tH8nIowuXyF7xNSH4LeEUZLqDr9VkYJhk38RPDMuc99fp\npsEZsTpmC56ygRZS/cVObZjefoKkl+2cPL0jfSWUSn2t/nUKbmwNbjbM6f1OCCyJ\n814LZalfeLh+hfEdY8yVK0Rlaut/FLw4eaY43H0QTwDyYXmgOgYckia7MQmtu5yO\n0dFRDZa7x0j01w0+Hphr88bhtXFgv8VPWyzyz2d8fNWlbMP4JCARes3lVpkMKHDn\nFZIULcECgYEA53qsuOQrE3xHtNzfak7FjGgkQTxi4TeYXRJ5Zhlkjtb9nQnsFzTq\n+AXpeHfsGmaza9/LcMUJ9JzYV7t5V8T6vZT3riOnjQAfbHOrR3EYK5/vv4ZfjFgk\nBySoNjOtf67ZXn1lRJ3FZNN4RfGmh/2aqYi5PAn1g5+nLXtMIY88a1ECgYEAxGp9\nW03pH4QwBzvFA9PJ+BhWXS8fAcafY67TlXg+IxzGP/YQupodovDChXpvbfYV0kf6\nU6YddDQT5p0j2YmssJCaDa93iMXU2/JgI5pcTNFxLa5LR/Mg30JYyniK/4+YIkoL\n68V2OH0Y65DIQdajHPojzFBmDSBZSnkyO9Ojx2MCgYADCoSMZUr/lYlnoeM5hVFp\nF9EqHj36UX2p810u7zR3//ETCBdW8rYHjiRUFdc/PYwr5aPJln0b/peFB4x/j7Hv\nna5nVkaUPqUrCpX8eUrk/9Ppgz1sHZhTk7K2C5XC8KwgZqtW7G+0dGbHHHagoL9Q\nbOBqHoNgOE+89Dq60iPsEQKBgByahXbueaylS3lCMwbDqP4ATVN0sUdI7Z1OsHFr\n+WCTqCtYYkdKelZoSWu20NNqqvLcmI/l+RQbIWrMJ5RegE+WP1kO3JGGfeEqAuYs\nbJSjS6AjacMonPjmaJfTxipBdx5HOkUzlGvVi/OCOiecYlSt+NigPLxcoaQ+0hn0\nUD2RAoGAUH2QV31iSFLs+ByQdGmPaYuVxhNUwaGoN0M5splIdH6DAgyIvM5ogXVb\nMV3yUdnULU+5YeL8r93NeFRHiBCBfvcIcDdJHxRJNQFvdsAWosGeXI6clC1YkUnB\n2N+9PrizN87XUb6NmY07NwtAKrU6aZ0XwKUEgor9rntT8IawFo4=\n-----END RSA PRIVATE KEY-----"
  }
};
var EventEmitter = require('events').EventEmitter;

describe('ClientIO', function () {
  this.timeout(200);
  
  it('requires a private key', function () {
    assert.throws(
      function () {
        new ClientIO();
      },
      /private key is required/
    );
  });
  
  it('accepts private key', function () {
    new ClientIO(testData.client.privateKey);
  });
  
  var target;
  var thisSideSocket;
  var otherSideSocket;
  
  beforeEach(function () {
    target = new ClientIO(testData.stack.privateKey);
    
    var bus = new EmittersBus();
    thisSideSocket = bus.createEmitter();
    otherSideSocket = bus.createEmitter();
  });
  
  describe('.listenSocket', function () {
    it('requires a socket', function () {
      assert.throws(
        function () {
          target.listenSocket();
        },
        /socket is required/
      );
    });
    
    it('accepts socket', function () {
      var socket = new EventEmitter();
      target.listenSocket(socket);
    });
    
    it('uses RSA socket', function (done) {
      var otherSideRsa = new RSASocket(testData.stack.privateKey);
      
      otherSideRsa.connect(otherSideSocket)
      .then(function () {
        done();
      });
      
      target.listenSocket(thisSideSocket);
    });
    
    it('uses RSA socket', function (done) {
      var otherSideRsa = new RSASocket(testData.stack.privateKey);
      
      otherSideRsa.connect(otherSideSocket)
      .then(function () {
        done();
      });
      
      target.listenSocket(thisSideSocket);
    });
    
    it('uses penelope over RSA', function (done) {
      var otherSideRsa = new RSASocket(testData.stack.privateKey);
      
      otherSideRsa.connect(otherSideSocket)
      .then(function (rsaSocket) {
        var penelope = new Penelope(rsaSocket);
        
        penelope
        .addHandler(
          'crane',
          function (socket, params) {
            try {
              assert(socket);
              assert
              .deepEqual(
                params,
                {
                  namespace: 'Client'
                }
              );
              
              done();
            } catch (e) {
              done(e);
            }
            
            return Promise.reject();
          }
        );
      });
      
      target.listenSocket(thisSideSocket);
    });
    
    it('resolves client with apis', function (done) {
      var otherSideRsa = new RSASocket(testData.stack.privateKey);
      
      otherSideRsa.connect(otherSideSocket)
      .then(function (rsaSocket) {
        var nr = new NameRegistry(testData.stack.privateKey);
        nr.addPublicNamespace('Client');
        
        new Crane(
          nr,
          function (socket) {
            socket.on('describe-apis', function (callback) {
              callback(
                null,
                {
                  Plugin1: {
                    method1: ['param1']
                  }
                }
              );
            });
          },
          rsaSocket
        );
      });
      
      target
      .listenSocket(thisSideSocket)
      .then(function (client) {
        try {
          assert(client);
          assert(client.Plugin1);
          assert(client.Plugin1.method1);
          
          done();
        } catch (e) {
          done(e);
        }
      });
    });
    
    it('updates apis on apis-changed event', function (done) {
      var otherSideRsa = new RSASocket(testData.stack.privateKey);
      var stackClientSideSocket;
      
      otherSideRsa.connect(otherSideSocket)
      .then(function (rsaSocket) {
        var nr = new NameRegistry(testData.stack.privateKey);
        nr.addPublicNamespace('Client');
        
        new Crane(
          nr,
          function (socket) {
            stackClientSideSocket = socket;
            
            socket.on('describe-apis', function (callback) {
              callback(
                null,
                {
                  Plugin1: {
                    method1: ['param1']
                  }
                }
              );
            });
          },
          rsaSocket
        );
      });
      
      target
      .listenSocket(thisSideSocket)
      .then(function (client) {
        stackClientSideSocket.emit('apis-changed', { 'Plugin2': { 'method2': [ 'param2 '] } });
        
        setTimeout(function () {
          try {
            assert(!client.Plugin1);
            assert(client.Plugin2);
            
            done();
          } catch (e) {
            done(e);
          }
        }, 100);
      });
    });
    
    it('rejects client if describe-apis callback fails', function (done) {
      var otherSideRsa = new RSASocket(testData.stack.privateKey);
      
      otherSideRsa.connect(otherSideSocket)
      .then(function (rsaSocket) {
        var nr = new NameRegistry(testData.stack.privateKey);
        nr.addPublicNamespace('Client');
        
        new Crane(
          nr,
          function (socket) {
            socket.on('describe-apis', function (callback) {
              callback(
                'some error'
              );
            });
          },
          rsaSocket
        );
      });
      
      target
      .listenSocket(thisSideSocket)
      .catch(function (err) {
        try {
          assert.equal(err, 'some error');
          
          done();
        } catch (e) {
          done(e);
        }
      });
    });
    
    it('client emits api events', function (done) {
      var otherSideRsa = new RSASocket(testData.stack.privateKey);
      
      otherSideRsa.connect(otherSideSocket)
      .then(function (rsaSocket) {
        var nr = new NameRegistry(testData.stack.privateKey);
        nr.addPublicNamespace('Client');
        
        new Crane(
          nr,
          function (socket) {
            socket
            .on('describe-apis', function (callback) {
              callback(
                null,
                {
                  'Plugin1': {
                    'method1': ['param1']
                  }
                }
              );
            });
            
            socket
            .on('api', function (stackToken, pluginName, methodName, methodParams, callback) {
              try {
                assert(pluginName, 'Plugin1');
                assert(methodName, 'method1');
                assert.deepEqual(
                  methodParams,
                  {
                    param1: 'magic'
                  }
                );
                assert(callback);
                assert(typeof(callback), 'function');
                
                done();
              } catch (e) {
                done(e);
              }
            });
          },
          rsaSocket
        );
      });
      
      target
      .listenSocket(thisSideSocket)
      .then(function (client) {
        client.Plugin1.method1('magic');
      });
    });
    
    it('rejects method invokation on api callback with err', function (done) {
      var otherSideRsa = new RSASocket(testData.stack.privateKey);
      
      otherSideRsa.connect(otherSideSocket)
      .then(function (rsaSocket) {
        var nr = new NameRegistry(testData.stack.privateKey);
        nr.addPublicNamespace('Client');
        
        new Crane(
          nr,
          function (socket) {
            socket
            .on('describe-apis', function (callback) {
              callback(
                null,
                {
                  'Plugin1': {
                    'method1': ['param1']
                  }
                }
              );
            });
            
            socket
            .on('api', function (stackToken, pluginName, methodName, methodParams, callback) {
              callback('some error');
            });
          },
          rsaSocket
        );
      });
      
      target
      .listenSocket(thisSideSocket)
      .then(function (client) {
        client.Plugin1.method1()
        .catch(function (err) {
          try {
            assert.equal(err, 'some error');
            
            done();
          } catch (e) {
            done(e);
          }
        });
      });
    });
    
    it('resolves method invokation on api callback with result', function (done) {
      var otherSideRsa = new RSASocket(testData.stack.privateKey);
      
      otherSideRsa.connect(otherSideSocket)
      .then(function (rsaSocket) {
        var nr = new NameRegistry(testData.stack.privateKey);
        nr.addPublicNamespace('Client');
        
        new Crane(
          nr,
          function (socket) {
            socket
            .on('describe-apis', function (callback) {
              callback(
                null,
                {
                  'Plugin1': {
                    'method1': ['param1']
                  }
                }
              );
            });
            
            socket
            .on('api', function (stackToken, pluginName, methodName, methodParams, callback) {
              callback(null, 'my result');
            });
          },
          rsaSocket
        );
      });
      
      target
      .listenSocket(thisSideSocket)
      .then(function (client) {
        client.Plugin1.method1()
        .then(function (result) {
          try {
            assert.equal(result, 'my result');
            
            done();
          } catch (e) {
            done(e);
          }
        });
      });
    });
    
    it('passes token', function (done) {
      var otherSideRsa = new RSASocket(testData.stack.privateKey);
      var theToken = { date: Date.now() };
      
      otherSideRsa.connect(otherSideSocket)
      .then(function (rsaSocket) {
        var nr = new NameRegistry(testData.stack.privateKey);
        nr.addPublicNamespace('Client');
        
        new Crane(
          nr,
          function (socket) {
            socket
            .on('describe-apis', function (callback) {
              callback(
                null,
                {
                  'Plugin1': {
                    'method1': ['param1']
                  }
                }
              );
            });
            
            socket
            .on('api', function (stackToken, pluginName, methodName, methodParams, callback) {
              try {
                assert.deepEqual(stackToken, theToken);
                
                done();
              } catch (e) {
                done(e);
              }
            });
          },
          rsaSocket
        );
      });
      
      target
      .listenSocket(thisSideSocket, theToken)
      .then(function (client) {
        client.Plugin1.method1()
        .then(function (result) {
          try {
            assert.equal(result, 'my result');
            
            done();
          } catch (e) {
            done(e);
          }
        });
      });
    });
  });
  
  describe('.createFactorySocket', function () {
    it('requires a socket', function () {
      assert.throws(
        function () {
          target.createFactorySocket();
        },
        /socket is required/
      );
    });
    
    it('creates a client with apis', function (done) {
      var otherSideRsa = new RSASocket(testData.stack.privateKey);
      
      otherSideRsa.connect(otherSideSocket)
      .then(function (rsaSocket) {
        var nr = new NameRegistry(testData.stack.privateKey);
        nr.addPublicNamespace('Client');
        
        new Crane(
          nr,
          function (socket) {
            socket.on('describe-apis', function (callback) {
              callback(
                null,
                {
                  Plugin1: {
                    method1: ['param1']
                  }
                }
              );
            });
          },
          rsaSocket
        );
      });
      
      target
      .createFactorySocket(thisSideSocket)
      .then(function (factory) {
        factory()
        .then(function (client) {
          try {
            
            assert(client);
            assert(client.Plugin1);
            assert(client.Plugin1.method1);
            
            done();
          } catch (e) {
            done(e);
          }
        });
      });
    });
  })
});