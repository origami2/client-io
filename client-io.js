var o2 = require('origami2');
var Crane = o2.Crane;
var RSASocket = o2.RSASocket;
var ClientFactory = o2.ClientFactory;
var factory = new ClientFactory();
var NameRegistry = o2.NameRegistry;

function ClientIO(privateKey) {
  if (!privateKey) throw new Error('private key is required');
  
  this.privateKey = privateKey;
  this.nameRegistry = new NameRegistry(privateKey);
}

ClientIO.prototype.connect = function (url, token) {
  var self = this;

  return new Promise(function (resolve, reject) {
    var socket = require('socket.io-client')(url);
    
    socket
    .on(
      'connect',
      function () {
        self
        .listenSocket(socket, token)
        .then(resolve)
        .catch(reject);
      }
    );
  });
};

ClientIO.prototype.createFactorySocket = function (socket) {
  if (!socket) throw new Error('socket is required');
  
  var self = this;

  return new Promise(function (resolve, reject) {
    var rsaSocket = new RSASocket(self.privateKey);
      
    rsaSocket
    .connect(socket)
    .then(function (secureSocket) {
      (new Crane(
        self.nameRegistry,
        function () {
          // no incoming connections
          return Promise.reject();
        },
        secureSocket
      ))
      .createSocket(
        'Client',
        function (subsocket, params) {
          resolve(function (token) {
            return factory.createClient(subsocket, token);
          });
        }
      )
      .catch(reject);
    });
  });
};

ClientIO.prototype.listenSocket = function (socket, token) {
  var self = this;
  
  if (!socket) throw new Error('socket is required');
  
  return new Promise(function (resolve, reject) {
    try {
      self
      .createFactorySocket(socket)
      .then(function (factory) {
        factory(token)
        .then(resolve)
        .catch(reject);
      })
      .catch(reject);
    } catch (e) {
      reject(e);
    }
  });
};

module.exports = ClientIO;